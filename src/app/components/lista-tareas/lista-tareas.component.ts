import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ITareas } from 'src/app/models/itareas';
import { ListaTareasService } from 'src/app/services/lista-tareas.service';

@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.scss']
})
export class ListaTareasComponent implements OnInit {

  // Lista de tareas que se rellena con los datos que nos devuelve el servicio
  tareas: ITareas[] = [];
  creaform!: FormGroup;
  fb: FormBuilder = new FormBuilder;


  constructor(public tareasService: ListaTareasService) {
    this.resetForm();
  }

  resetForm(){
    this.creaform = this.fb.group({
      id: ['', Validators.required],
      user_id: ['', [Validators.required]],
      title: ['', Validators.required],
      due_on: ['', Validators.required],
      status: ['', Validators.required]
    });
  }

  agregarTarea() {
    let nuevaTarea: ITareas = {...this.creaform.value};
    this.tareas.push(nuevaTarea);
    this.resetForm();
  }

  eliminarTarea(id: ITareas) {
    const index = this.tareas.findIndex((elemento: ITareas) => elemento === id);
    this.tareas.splice(index, 1);
  }

  ngOnInit(): void {
    this.tareasService.getTareas().subscribe((respuesta) => (this.tareas = respuesta.data));
  }

}

